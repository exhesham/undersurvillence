module.exports.exposed = {
    // internal javascript
    "":{loginRequired:true,name:"./WebClient/index.html"},
    "/":{loginRequired:true,name:"./WebClient/index.html"},
    "index.html":{loginRequired:true,name:"./WebClient/index.html"},
    "login.html":{loginRequired:true,name:"./WebClient/login.html"},
    "login.js":{loginRequired:false,name:"./WebClient/js/login.js"},
    "index.js":{loginRequired:false,name:"./WebClient/js/index.js"},
    "jquery.js":{loginRequired:false,name:"./WebClient/js/jquery.js"},
    "bootstrap.min.js":{loginRequired:false,name:"./WebClient/js/bootstrap.min.js"},
    "style.css":{loginRequired:false,name:"./WebClient/css/style.css"},
    "portfolio-item.css":{loginRequired:false,name:"./WebClient/css/portfolio-item.css"},
    "bootstrap.min.css":{loginRequired:false,name:"./WebClient/css/bootstrap.min.css"},
    "bootstrap.min.css.map":{loginRequired:false,name:"./WebClient/css/bootstrap.min.css.map"},


    "favicon.ico":{loginRequired:false,name:"./WebClient/images/favicon.ico"}
};
